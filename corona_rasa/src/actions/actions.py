# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.events import AllSlotsReset
from rasa_sdk.executor import CollectingDispatcher

sources = {
    "trusted": ["bbc", "nhs", "who","cdc"],
    "untrusted": ["facebook", "twitter", "youtube", "whatsapp", "telegram", "instagram", "reddit", "friend", "family"]
}

actual_name = {
    "bbc": "BBC",
    "nhs": "NHS",
    "who": "WHO",
    "cdc": "CDC",
    "facebook": "Facebook",
    "twitter": "Twitter",
    "youtube": "Youtube",
    "whatsapp": "Whatsapp",
    "telegram": "Telegram",
    "instagram": "Instagram",
    "reddit":"Reddit",
    "friend":"Friend",
    "family": "Family"
}


# class ActionHelloWorld(Action):

#     def name(self) -> Text:
#         return "action_test"

#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

#         dispatcher.utter_message(template="utter_list_myths")

#         return []


# LOGICAL -> CREDIBILITY -> EMOTIONAL if skeptic
# CREDIBILITY -> EMOTIONAL if conspiracy

#looks at whatevers in myth_type slot, returns a credibility appeal based on that
class ActionAppealCredibility(Action):
    def name(self) -> Text:
        return "action_appeal_credibility"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        #5g CREDIBILITY
        if tracker.get_slot("myth_type") == "5G" or tracker.get_slot("myth_type") == "5g":
            # 5G 3 main subtypes
            if tracker.get_slot("5g_subtype") == "spreadCovid":
                dispatcher.utter_message(template="utter_cred_5g_spreadsCovid")
            elif tracker.get_slot("5g_subtype") == "dangerous":
                dispatcher.utter_message(template="utter_cred_5g_dangerous")
            elif tracker.get_slot("5g_subtype") == "health":
                dispatcher.utter_message(template="utter_cred_5g_health")
            ### 5G one offs CREDIBILITY
            elif tracker.get_slot("5g_subtype") == "5gSymptoms":
                dispatcher.utter_message(template="utter_5g_Symptoms")
            elif tracker.get_slot("5g_subtype") == "towers":
                dispatcher.utter_message(template="utter_5g_Towers")
            elif tracker.get_slot("5g_subtype") == "infect":
                dispatcher.utter_message(template="utter_5g_infectsPeople")
           
            else:        #base myth        
                dispatcher.utter_message(template="utter_appeal_5g_credibility")

        #vaccine CREDIBILITY
        elif tracker.get_slot("myth_type") == "vaccine" or tracker.get_slot("myth_type") == "vaccines":

            #vaccine 3 main subtypes
            if tracker.get_slot("vaccine_subtype") == "giveCovid":
                dispatcher.utter_message(template="utter_cred_vaccine_giveCovid") 
            elif tracker.get_slot("vaccine_subtype") == "trackers":
                dispatcher.utter_message(template="utter_cred_vaccine_trackers")
            elif tracker.get_slot("vaccine_subtype") == "autism":
                dispatcher.utter_message(template="utter_cred_vaccine_autism")
            #### vaccine one offs CREDIBILITY
            elif tracker.get_slot("vaccine_subtype") == "microchips":
                dispatcher.utter_message(template="utter_vaccines_microchipsTracking")
            elif tracker.get_slot("vaccine_subtype") == "safety":
                dispatcher.utter_message(template="utter_vaccines_safety")
            elif tracker.get_slot("vaccine_subtype") == "coverup":
                dispatcher.utter_message(template="utter_vaccines_coverup")
            elif tracker.get_slot("vaccine_subtype") == "control":
                dispatcher.utter_message(template="utter_vaccines_control")
            elif tracker.get_slot("vaccine_subtype") == "dna":
                dispatcher.utter_message(template="utter_vaccines_altersDNA")
            
            else:                #base case
                dispatcher.utter_message(template="utter_appeal_vaccine_credibility")
        
        #masks CREDIBILITY
        elif tracker.get_slot("myth_type") == "mask"or tracker.get_slot("myth_type") == "masks":
           
            # masks 3 main subtypes
            if tracker.get_slot("mask_subtype") == "freedom":
                dispatcher.utter_message(template="utter_cred_masks_rightsAndFreedom")
            elif tracker.get_slot("mask_subtype") == "suffocation":
                dispatcher.utter_message(template="utter_cred_masks_suffocation")
            elif tracker.get_slot("mask_subtype") == "effectiveness":
                dispatcher.utter_message(template="utter_cred_masks_notEffective")
            
            ### Mask one offs CREDIBILITY
            elif tracker.get_slot("mask_subtype") == "spreadCovid":
                dispatcher.utter_message(template="utter_masks_spreadCovid")
            elif tracker.get_slot("mask_subtype") == "tracking":
                dispatcher.utter_message(template="utter_masks_tracking")
            elif tracker.get_slot("mask_subtype") == "killYou":
                dispatcher.utter_message(template="utter_masks_killYou")
            elif tracker.get_slot("mask_subtype") == "censorship":
                dispatcher.utter_message(template="utter_masks_censorship")
            elif tracker.get_slot("mask_subtype") == "sickness":
                dispatcher.utter_message(template="utter_masks_causeSickness")
       
            else:        #base myth        
                dispatcher.utter_message(template="utter_appeal_mask_credibility")
       
        return []



#looks at whatevers in myth_type slot, returns a logical appeal based on that
class ActionAppealLogical(Action):

    def name(self) -> Text:
        return "action_appeal_logical"
    
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        #5g LOGICAL
        if tracker.get_slot("myth_type") == "5G" or tracker.get_slot("myth_type") == "5g":
            # Main 3 5G subtypes
            if tracker.get_slot("5g_subtype") == "spreadCovid":
                dispatcher.utter_message(template="utter_5g_spreadCovid_logical")
            elif tracker.get_slot("5g_subtype") == "dangerous":
                dispatcher.utter_message(template="utter_5g_dangerous_logical")
            elif tracker.get_slot("5g_subtype") == "health":
                dispatcher.utter_message(template="utter_5g_health_logical")
            # 5G one off responses LOGICAL
            elif tracker.get_slot("5g_subtype") == "control":
                dispatcher.utter_message(template="utter_5g_takingControl")
            elif tracker.get_slot("5g_subtype") == "infect":
                dispatcher.utter_message(template="utter_5g_infectsPeople")
                
            else:        #base myth        
                dispatcher.utter_message(template="utter_appeal_5g_logical")

        #vaccine LOGICAL
        elif tracker.get_slot("myth_type") == "vaccine" or tracker.get_slot("myth_type") == "vaccines":
           
            # Main 3 vaccine subtypes LOGICAL
            if tracker.get_slot("vaccine_subtype") == "giveCovid":
                dispatcher.utter_message(template="utter_vaccines_givesVirus_logical")
            elif tracker.get_slot("vaccine_subtype") == "trackers":
                dispatcher.utter_message(template="utter_vaccines_trackers_logical")
            elif tracker.get_slot("vaccine_subtype") == "autism":
                dispatcher.utter_message(template="utter_vaccines_autism_logical")
            # Vaccine one off responses LOGICAL  
            elif tracker.get_slot("vaccine_subtype") == "dna":
                dispatcher.utter_message(template="utter_vaccines_altersDNA")
            elif tracker.get_slot("vaccine_subtype") == "effectiveness":
                dispatcher.utter_message(template="utter_vaccines_effectiveness")
            elif tracker.get_slot("vaccine_subtype") == "microchips":
                dispatcher.utter_message(template="utter_vaccines_microchipsTracking")
            elif tracker.get_slot("vaccine_subtype") == "safety":
                dispatcher.utter_message(template="utter_vaccines_safety")
            
            else:        #base myth        
                dispatcher.utter_message(template="utter_appeal_vaccine_logical")
            
        #mask LOGICAL  
        elif tracker.get_slot("myth_type") == "mask" or tracker.get_slot("myth_type") == "masks":
           
            # Main 3 mask subtypes LOGICAL
            if tracker.get_slot("mask_subtype") == "freedom":
                dispatcher.utter_message(template="utter_masks_rightsFreedom_logical")
            elif tracker.get_slot("mask_subtype") == "suffocation":
                dispatcher.utter_message(template="utter_masks_suffocation_logical")
            elif tracker.get_slot("mask_subtype") == "effectiveness":
                dispatcher.utter_message(template="utter_masks_notEffective_logical")
            #Mask one off responses LOGICAL
            
            elif tracker.get_slot("mask_subtype") == "distancing":
                dispatcher.utter_message(template="utter_masks_distancing")
            elif tracker.get_slot("mask_subtype") == "sickness":
                dispatcher.utter_message(template="utter_masks_causeSickness")
            
            else:        #base myth        
                dispatcher.utter_message(template="utter_appeal_mask_logical")

        return []


#looks at whatevers in myth_type slot, returns an emotional appeal based on that
class ActionAppealEmotional(Action):

    def name(self) -> Text:
        return "action_appeal_emotional"
    
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        #5g
        if tracker.get_slot("myth_type") == "5G" or tracker.get_slot("myth_type") == "5g":
            
            #vaccine subtypes
            if tracker.get_slot("5g_subtype") == "spreadCovid":
                dispatcher.utter_message(template="utter_emot_5g_spreadCovid")
            elif tracker.get_slot("5g_subtype") == "dangerous":
                dispatcher.utter_message(template="utter_emot_5g_dangerous")
            elif tracker.get_slot("5g_subtype") == "health":
                dispatcher.utter_message(template="utter_emot_5g_health")
            else:        #base myth        
                dispatcher.utter_message(template="utter_appeal_5g_emotional")


        #vaccine
        elif tracker.get_slot("myth_type") == "vaccine" or tracker.get_slot("myth_type") == "vaccines":
            #vaccine subtypes
            if tracker.get_slot("vaccine_subtype") == "giveCovid":
                dispatcher.utter_message(template="utter_emot_vaccine_giveCovid")
            elif tracker.get_slot("vaccine_subtype") == "trackers":
                dispatcher.utter_message(template="utter_emot_vaccine_trackers")
            elif tracker.get_slot("vaccine_subtype") == "autism":
                dispatcher.utter_message(template="utter_emot_vaccine_autism")
            else:        #base myth
                dispatcher.utter_message(template="utter_appeal_vaccine_emotional")
    
        #masks
        elif tracker.get_slot("myth_type") == "masks" or tracker.get_slot("myth_type") == "mask":
            #Masks subtype
            if tracker.get_slot("mask_subtype") == "freedom":
                dispatcher.utter_message(template="utter_emot_masks_rightsAndFreedom")
            elif tracker.get_slot("mask_subtype") == "suffocation":
                dispatcher.utter_message(template="utter_emot_masks_suffocation")
            elif tracker.get_slot("mask_subtype") == "effectiveness":
                dispatcher.utter_message(template="utter_emot_masks_notEffective")
            else:        #base myth        
                dispatcher.utter_message(template="utter_appeal_mask_emotional")
        return []



# If the source is social media, tell them its not trustworthy. getGoodSource tells user some sites for expert advice on covid
class ActionWhichSource(Action):

    def name(self) -> Text:
        return "action_get_news_source"
    
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        if tracker.get_slot("source") in sources.get("untrusted"):
            if tracker.get_slot("source") in ["friend", "family"]:
                dispatcher.utter_message(template="utter_source_dispute")
                dispatcher.utter_message(template="utter_good_source")
            else:
                dispatcher.utter_message(template="utter_bad_source")

        elif tracker.get_slot("source") == "getGoodSource":
            dispatcher.utter_message(template="utter_good_source")
        elif tracker.get_slot("source") in sources.get("trusted"):
            dispatcher.utter_message(text=actual_name.get(tracker.get_slot("source")) + 
            " is considered a reliable source, and information provided should be accurate and up-to-date.")
        else:
            dispatcher.utter_message(template="utter_uncertain_source")
        return []

class ActionResetAllSlots(Action):
    def name(self) -> Text:
        return "action_reset_all_slots"
    
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        return [AllSlotsReset()]
#!/bin/bash
#

# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
echo $SCRIPTPATH
PROJECTROOTPATH="$(dirname "$SCRIPTPATH")"
echo $PROJECTROOTPATH

source ${SCRIPTPATH}/ENV/bin/activate

MODEL_PATH=${SCRIPTPATH}/src/models

echo $MODEL_PATH
# making sure the folder exists, otherwise it will be created
if [[ ! -e "${MODEL_PATH}" ]]; then
    mkdir "${MODEL_PATH}"
elif [[ ! -d "${MODEL_PATH}" ]]; then
    echo "${MODEL_PATH} already exists but is not a directory" 1>&2
fi

cd "${SCRIPTPATH}"/src
rasa train

exec $SHELL
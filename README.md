# Setup of corona bot with rasa

First use `git pull` to ensure you are on the lastest version of the corona bot.


The first time using the bot, run the setup script in the `corona_rasa` directory to setup the virtual environment and download requirements.


Go into the file `corona_rasa/src/endpoints.yml` and make sure that `action_endpoint` appears like this, and is uncommented:
```yaml
action_endpoint:
 url: "http://localhost:5056/webhook"
```

NOTE: The port has been changed from 5055 to 5056 (not sure if this matters)


Open a terminal in `corona_rasa`, activate the virtualenv using the command `source ENV/bin/activate`, next type command `cd src`,  now train the nlu and core
using the command `rasa train`. Training time depends on number of epochs specified in the `config.yml` file.

# Running the bot

Make sure you're in the `corona_rasa/src` directory for all the following, and that the virtual environment is running on each
terminal you run. if you're in `src` start the venv using `source ../ENV/bin/activate`.


Once trained, first get the custom action server running by opening a terminal and typing the command `rasa run actions --port 5056`.


Open another terminal and type the command `rasa shell`, this will bring up the interactive bot.


To check the nlu, open a terminal and type the command `rasa shell nlu`.

## Connect to telegram

'ngrok http 5005'
Add webhook_url line in creditials.yml with correct ngrok url
'rasa run'
make sure action server is running to
